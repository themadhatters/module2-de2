/*
 * sound_helper.h
 *
 *  Created on: 2014-04-06
 *      Author: 1st
 */

#ifndef SOUND_HELPER_H_
#define SOUND_HELPER_H_

#include "music.h"
#include "message.h"


void makeByteData(void);
//@effect: make byte data one time for all 16 sounds

void normalize(int i);
//@effect: normalize all inputs

void resetDataCounters(void);
//@effect: reset data counter one time for all 16 sounds


#endif /* SOUND_HELPER_H_ */
