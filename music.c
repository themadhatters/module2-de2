/*
 * music.c
 *
 *  Created on: 2014-01-23
 *      Author: 1st//
 *
 *      - Song sample: need 16-bit, 512kbps sound samples
 *		- Read byte by byte from SD car, concatenate 2 bytes to make one 16bit amplitude sample,
 *		  write to the audio buffer 2 bytes at a time, write the same data to left and right outputs (mono).
 *		- If 2 songs are playing, normalize them by dividing both of their amplitudes by 2 and adding them together.
 *		- 2 bytes read from the wave file contain 1 amplitude point in the whole sound wave. Their reading speed determine
 *		  the frequency.
 *		- Implement a circular buffer. Use a regular array and index i. Whenever i++, make i%L where L = length of array.
 *
 */

#include "music.h"


alt_up_audio_dev *  av_config_setup() {
	//@effects: Initialize audio device

	alt_up_av_config_dev* audio_config;

	audio_config = alt_up_av_config_open_dev("/dev/audio_and_video_config_0");
	if (audio_config == NULL)
		printf("ERROR in opening audio config\n");
	else
		printf("Opened audo config\n");

	while (!alt_up_av_config_read_ready(audio_config)) {
	}

	// open the Audio port
	alt_up_audio_dev * audio_dev = alt_up_audio_open_dev("/dev/audio_0");
	if (audio_dev == NULL)
		printf("Error: could not open audio device \n");
	else
		printf("Opened audio device \n");

	return audio_dev;
}

void audio_irq_init(alt_up_audio_dev *audio_dev){
	//@effects: initialize irq (interrupt request) for audio write

	alt_up_audio_disable_read_interrupt(audio_dev);
	alt_up_audio_disable_write_interrupt(audio_dev);
	printf("Audio IRQ Initialized...\n");
}

void load_music_header(void){
	//@effects: load the wav file header to music buffer
	music_handle = alt_up_sd_card_fopen("music.wav", 0);
	printf("music_handle: %i \n", music_handle );

	if (header_bit == 0){
		// skip header
		int i;
		for (i = 0; i < HEADER_SIZE; i++)
		{
			alt_up_sd_card_read(music_handle);
		}
		header_bit = 1;
	}
}

void close_music(void){
	//@effects: close music file
	alt_up_sd_card_fclose(music_handle);
}

void load_s1_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s1.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s1_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s2_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s2.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s2_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s3_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s3.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s3_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s4_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s4.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s4_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s5_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s5.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s5_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s6_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s6.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s6_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s7_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s7.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s7_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s8_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s8.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s8_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s9_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s9.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s9_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s10_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s10.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s10_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s11_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s11.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s11_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s12_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s12.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s12_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s13_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s13.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			//printf("tmp[%d]=%d\n",i,tmp);
			s13_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s14_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s14.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");
	//printf("\n\n shortint size : %u\n\n", sizeof(tmp));
	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			s14_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s15_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s15.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");

	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			s15_sound[i] = tmp;// & 0x00FF;
			i++;
		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}

void load_s16_body(void){
	//@effects: load the jump wav file to music buffer

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("s16.wav", 0);
	printf("file_handle: %i \n", file_handle);

	short int tmp;
	int i = 0;
	printf("Starting to read sound file\n");

	while (1)
	{
		tmp = alt_up_sd_card_read(file_handle);
		if (tmp == -1){
			printf("end of file\n");
			break;
		}
		else{
			s16_sound[i] = tmp;// & 0x00FF;
			i++;

		}
	}

	printf("i = %d\n", i);
	alt_up_sd_card_fclose(file_handle);
}
void test_isr(void* context, alt_u32 id){
	//@effects:

	int i;
	for (i = 0; i < SAMPLE_SIZE; i++)
	{
		// Make 2 bytes
		makeByteData();

		// Normalization and combine the two bytes and store into sample buffer//
		normalize(i);

		// resetting data counters
		resetDataCounters();
	}

	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(context, normal_buff, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(context, normal_buff, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}

void play_sound(alt_up_audio_dev *audio_dev){
	//@effects: call this function to play music

	// register isr
	sound_data_counter = 0;
	alt_irq_register(AUDIO_0_IRQ, audio_dev, test_isr);

	// enable interrupt
	alt_irq_enable(AUDIO_0_IRQ);
	alt_up_audio_enable_write_interrupt(audio_dev);
}
