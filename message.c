/*
 * message.c
 *
 *  Created on: 2014-03-26
 *      Author: x9a8
 */



#include "message.h"
//*************************************************************************************************************************************************//
//REQUIRE: Message[] must contain the signal received from android devices, uart cannot be null
//MODIFY:  nothing
//EFFECT:  Broadcasts received message to all connected android devices.
//         Be careful when you are sending a string from DE2 to android. A string in C is ended with a null character while
//         a string in Java is not terminated with a null character. So when sending a string from DE2 to Java, ONLY SEND THE CHARACTERS!
//*************************************************************************************************************************************************//

void send_msg(alt_up_rs232_dev *uart, unsigned char *input_stream, unsigned char *parity,unsigned char *message,int *inputSize){
	/**************** Adding a null character to make message a string ****************/
	message[1] = '\0';
	//printf("message=%s\n",message);

	/**************** Sending message ****************/
	//printf("Sending the message to the Middleman\n");
	// Write the first byte (Client ID), 0xff if broadcasting
	alt_up_rs232_write_data(uart,(unsigned char)0xff);
	// Write the second byte (size of message)
	//printf("After write data\n");
	//Size=number of char characters in the array.
	alt_up_rs232_write_data(uart, (unsigned char)1);
	// Write the message( ONLY SEND THE CHAR )
	alt_up_rs232_write_data(uart, message[0]);

	/**************** Clearing message array ****************/
	//memset(message, 0 , strlen((char*)message));
	message[0] = (unsigned char)NULL;
	//printf("Sending complete\n");
}


//*************************************************************************************************************************************************//
//REQUIRE: uart cannot be null, Android must only send 1 char character at a time.
//MODIFY:  nothing
//EFFECT:  receive received message from android devices and store the char character at message[0]
//*************************************************************************************************************************************************//
void receive_msg(alt_up_rs232_dev *uart, unsigned char *input_stream, unsigned char *parity,unsigned char *message,int *inputSize){

	//***********Wait until the client ID is available***************************//
	while (alt_up_rs232_get_used_space_in_read_FIFO(uart) == 0);

	//***********Read Client ID************************************************//
	alt_up_rs232_read_data(uart,input_stream,parity);

	//************Wait til the next byte is available*****************//
	while (alt_up_rs232_get_used_space_in_read_FIFO(uart) == 0);

	//***********Do nothing with this byte***********************//
	alt_up_rs232_read_data(uart,input_stream,parity);

	//***********Wait til the size is available*****************//
	while (alt_up_rs232_get_used_space_in_read_FIFO(uart) == 0);

	//***********Read the size( # of char ) of the received message**********//
	alt_up_rs232_read_data(uart, input_stream, parity);
	inputSize = (int*)input_stream;

	//*************Wait til the message (char) is available**************//
	while (alt_up_rs232_get_used_space_in_read_FIFO(uart) == 0);

	//*************Read the char character and store it in message[0]***********//
	alt_up_rs232_read_data(uart, input_stream, parity);
	message[0]=*input_stream;
}


//*************************************************************************************************************************************************//
//REQUIRE: every entry of song_list has to be false
//MODIFY:  song_list
//EFFECT:  update song_list based on the message received from all the android devices
//*************************************************************************************************************************************************//
void check_song(int *song_list, unsigned char signal){
	if(signal=='a'){
		song_list[0]=1;
		s1_data_counter = 0;
		if(now_playing[0]==1 || song_list[0]==1)
			now_playing[0] = 1;
	}
	else if(signal=='b'){
		song_list[1]=1;
		s2_data_counter = 0;
		if(now_playing[1]==1 || song_list[1]==1)
			now_playing[1] = 1;
	}
	else if(signal=='c'){
		song_list[2]=1;
		s3_data_counter = 0;
		if(now_playing[2]==1 || song_list[2]==1)
			now_playing[2] = 1;
	}
	else if(signal=='d'){
		song_list[3]=1;
		s4_data_counter = 0;
		if(now_playing[3]==1 || song_list[3]==1)
			now_playing[3] = 1;
	}
	else if(signal=='e'){
		song_list[4]=1;
		s5_data_counter = 0;
		if(now_playing[4]==1 || song_list[4]==1)
			now_playing[4] = 1;
	}
	else if(signal=='f'){
		song_list[5]=1;
		s6_data_counter = 0;
		if(now_playing[5]==1 || song_list[5]==1)
			now_playing[5] = 1;
	}
	else if(signal=='g'){
		song_list[6]=1;
		s7_data_counter = 0;
		if(now_playing[6]==1 || song_list[6]==1)
			now_playing[6] = 1;
	}
	else if(signal=='h'){
		song_list[7]=1;
		s8_data_counter = 0;
		if(now_playing[7]==1 || song_list[7]==1)
			now_playing[7] = 1;
	}
	else if(signal=='i'){
		song_list[8]=1;
		s9_data_counter = 0;
		if(now_playing[8]==1 || song_list[8]==1)
			now_playing[8] = 1;
	}
	else if(signal=='j'){
		song_list[9]=1;
		s10_data_counter = 0;
		if(now_playing[9]==1 || song_list[9]==1)
			now_playing[9] = 1;
	}
	else if(signal=='k'){
		song_list[10]=1;
		s11_data_counter = 0;
		if(now_playing[10]==1 || song_list[10]==1)
			now_playing[10] = 1;
	}
	else if(signal=='l'){
		song_list[11]=1;
		s12_data_counter = 0;
		if(now_playing[11]==1 || song_list[11]==1)
			now_playing[11] = 1;
	}
	else if(signal=='m'){
		song_list[12]=1;
		s13_data_counter = 0;
		if(now_playing[12]==1 || song_list[12]==1)
			now_playing[12] = 1;
	}
	else if(signal=='n'){
		song_list[13]=1;
		s14_data_counter = 0;
		if(now_playing[13]==1 || song_list[13]==1)
			now_playing[13] = 1;
	}
	else if(signal=='o'){
		song_list[14]=1;
		s15_data_counter = 0;
		if(now_playing[14]==1 || song_list[14]==1)
			now_playing[14] = 1;
	}
	else if(signal=='p'){
		song_list[15]=1;
		s16_data_counter = 0;
		if(now_playing[15]==1 || song_list[15]==1)
			now_playing[15] = 1;
	}
	else if(signal=='x'){
		mute = 0;
	}
	else if(signal=='y'){
		mute = 1;
	}
}

//*************************************************************************************************************************************************//
//REQUIRE: none
//MODIFY:  song_list
//EFFECT:  set every entry in song_list to false. S
//*************************************************************************************************************************************************//
void reset_song_list(int *song_list){
	int i;
	for(i=0;i<16;i++)
		song_list[i]=0;
}
