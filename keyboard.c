/*
 * keyboard.c
 *
 *  Created on: 2014-04-05
 *      Author: yoonjib
 */

#include "keyboard.h"

alt_up_ps2_dev * init_keyboard(alt_up_ps2_dev *keyboard_device){
	keyboard_device=alt_up_ps2_open_dev(PS2_0_NAME);

	if(keyboard_device==NULL)
		printf("Failed to initialize keyboard!\n");
	else{
		alt_up_ps2_init(keyboard_device);
		printf("--------------Keyboard initialized!\n");
		alt_up_ps2_clear_fifo(keyboard_device);
	}

	return keyboard_device;
}

char keyboardInput(alt_up_ps2_dev* keyboard_device,KB_CODE_TYPE* decode_mode,alt_u8* temp_buf_Keyboard,int* keyEnabled){
	char ascii;
	//-----------------------Checking for User Input -------------------//
	if(decode_scancode(keyboard_device,decode_mode,temp_buf_Keyboard,&ascii)==0){
		if (*keyEnabled == 1) {
			*keyEnabled = 0;
		}
		if (ascii==0x00) {
			*keyEnabled = 1;
		}
	}
	else
		ascii='\0';
	return ascii;
}

char changeToPianoInput(char ascii){
	if(ascii==A)
		return 'q';
	else if(ascii==S)
		return 'r';
	else if(ascii==D)
		return 's';
	else if(ascii==F)
		return 't';
	else if(ascii==G)
		return 'u';
	else if(ascii==H)
		return 'v';
	else if(ascii==J)
		return 'w';
	else
		return '\0';
}
