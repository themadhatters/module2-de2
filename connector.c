/*********** Including standard C libraries ***********/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <system.h>
#include <io.h>
#include <stdlib.h>
#include "graph_init.h"
#include "display.h"
#include "music.h"
#include "sd_card.h"
#include "message.h"
#include "sound_helper.h"
#include "keyboard.h"

/*********** Including Altera hardware libraries ***********/
#include <altera_up_avalon_character_lcd.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>
#include <altera_up_avalon_video_character_buffer_with_dma.h>
#include <altera_up_avalon_rs232.h>
#include <altera_UP_SD_Card_Avalon_Interface.h>
#include <altera_up_avalon_audio.h>
#include <altera_up_avalon_audio_and_video_config.h>
#include <sys/alt_alarm.h>
#include <sys/alt_timestamp.h>
#include <alt_types.h>
#include <altera_up_avalon_ps2.h>		//ps2 keyboard
#include <altera_up_ps2_keyboard.h>		//ps2 keyboard (need both)

//Circular Buffer array
//We need 16 circular buffers, one for each sound
int song_list[16];
volatile int now_playing[16];		// the array sent to audio interrupt
int global_bit;
int mute; // 1 = mute, else = non-mute

// Pixel buffer variables
unsigned int pixel_buffer_addr1 = PIXEL_BUFFER_BASE;
unsigned int pixel_buffer_addr2 = PIXEL_BUFFER_BASE + (512 * 240 * 2);

// SD Card variables
unsigned char readout [1024];

// Music variables
int alt_irq_register(alt_u32 id, void* context, void (*isr)(void*, alt_u32));

//unsigned int sound_data [FILE_SIZE];
unsigned int sound_data_counter = 0; 	//check if reach end of segment
unsigned int* sound_buff;

short int signedTwoBytes [16];
unsigned int normal_buff[SAMPLE_SIZE];
unsigned short s1_sound [S1_SIZE];
unsigned int s1_data_counter = 0; 	//check if reach end of segment
//unsigned int s1_buff[SAMPLE_SIZE];
unsigned short s2_sound [S2_SIZE];
unsigned int s2_data_counter = 0; 	//check if reach end of segment
unsigned short s3_sound [S3_SIZE];
unsigned int s3_data_counter = 0; 	//check if reach end of segment
unsigned int s4_sound [S4_SIZE];
unsigned int s4_data_counter = 0; 	//check if reach end of segment
unsigned int s5_sound [S5_SIZE];
unsigned int s5_data_counter = 0; 	//check if reach end of segment
unsigned int s6_sound [S6_SIZE];
unsigned int s6_data_counter = 0; 	//check if reach end of segment
unsigned int s7_sound [S7_SIZE];
unsigned int s7_data_counter = 0; 	//check if reach end of segment
unsigned int s8_sound [S8_SIZE];
unsigned int s8_data_counter = 0; 	//check if reach end of segment
unsigned int s9_sound [S9_SIZE];
unsigned int s9_data_counter = 0; 	//check if reach end of segment
unsigned int s10_sound [S10_SIZE];
unsigned int s10_data_counter = 0; 	//check if reach end of segment
unsigned int s11_sound [S11_SIZE];
unsigned int s11_data_counter = 0; 	//check if reach end of segment
unsigned int s12_sound [S12_SIZE];
unsigned int s12_data_counter = 0; 	//check if reach end of segment
unsigned int s13_sound [S13_SIZE];
unsigned int s13_data_counter = 0; 	//check if reach end of segment
unsigned int s14_sound [S14_SIZE];
unsigned int s14_data_counter = 0; 	//check if reach end of segment
unsigned int s15_sound [S15_SIZE];
unsigned int s15_data_counter = 0; 	//check if reach end of segment
unsigned int s16_sound [S16_SIZE];
unsigned int s16_data_counter = 0; 	//check if reach end of segment

unsigned short int unsignedIntTwoBytes;


int header_bit = 0;
int finishedPlaying = 1;
short int music_handle;
unsigned int byte_data[2];

unsigned short byte_data_s1[2];
unsigned short byte_data_s2[2];
unsigned short byte_data_s3[2];
unsigned short byte_data_s4[2];
unsigned short byte_data_s5[2];
unsigned short byte_data_s6[2];
unsigned short byte_data_s7[2];
unsigned short byte_data_s8[2];
unsigned short byte_data_s9[2];
unsigned short byte_data_s10[2];
unsigned short byte_data_s11[2];
unsigned short byte_data_s12[2];
unsigned short byte_data_s13[2];
unsigned short byte_data_s14[2];
unsigned short byte_data_s15[2];
unsigned short byte_data_s16[2];

unsigned int end_file_counter = 0;		//check if reach end of music
unsigned int end_of_segment = 0;
unsigned int end_of_file = 0;

//***********************Global Variables for message***********************//
int inputSize;
unsigned char input_stream;		// char used to store one byte read from middleman
unsigned char parity;			// char used to store parity. Can use parity to check for errors.
unsigned char message[255];		// char array used to store the whole input stream
unsigned char clientID;			// char used to store the ID of the Android device

//------------------------KEYBOARD------------------------------//
char ascii;  //input from keyboard
char pianoInput;

int main()
{
	/* Declaring main variables */
	printf("Initializing... \n");
	int button_counter = 0;

	/* Initialize hardware */
	alt_up_pixel_buffer_dma_dev* pixel_buffer;	// Pixel buffer pointer
	alt_up_char_buffer_dev *char_buffer;
	alt_up_sd_card_dev *SD_CARD = NULL;
	alt_up_audio_dev *audio_dev;
	//	alt_up_character_lcd_dev * char_lcd_dev;	// LCD pointer
	//	alt_up_ps2_dev* keyboard_device;          	// Keyboard Pointer
	alt_u8 temp_buf_Keyboard;
	KB_CODE_TYPE decode_mode;
	alt_up_ps2_dev* keyboard_device;

	keyboard_device = init_keyboard(keyboard_device);
	int keyEnabled = 1;

	/* Timer Calibration */
	float duration;
	int ticks_start;
	int ticks_end;
	int ticks_per_s;
	int ticks_duration;
	int timer_period;
	ticks_per_s = alt_ticks_per_second();
	ticks_start = alt_nticks();
	usleep(100000);
	ticks_end = alt_nticks();
	ticks_duration = ticks_end - ticks_start;
	duration = (float) ticks_duration / (float) ticks_per_s;

	/* Initialize pixel buffer */
	pixel_buffer=graph_init(pixel_buffer);

	/* Initialize UART RS-232 */
	printf("UART Initialization\n");
	alt_up_rs232_dev* uart = alt_up_rs232_open_dev(RS232_0_NAME);

	/* Initialize SD card */
	SD_CARD = sd_card_init(SD_CARD);

	/* Initialize music player */
	audio_dev = av_config_setup();
	audio_irq_init(audio_dev);
	load_s1_body();
	load_s2_body();
	load_s3_body();
	load_s4_body();
	load_s5_body();
	load_s6_body();
	load_s7_body();
	load_s8_body();
	load_s9_body();
	load_s10_body();
	load_s11_body();
	load_s12_body();
	load_s13_body();
	load_s14_body();
	load_s15_body();
	load_s16_body();
	//TO-DO: load all sound files from SD card here
	play_sound(audio_dev);

	/* Clearing input buffer */
	printf("Clearing input buffer to start\n");
	while (alt_up_rs232_get_used_space_in_read_FIFO(uart)) {
		alt_up_rs232_read_data(uart, &input_stream, &parity);
	}

	/* Get starting ticks for broadcasting z */
	int z_start;
	int z_end;
	z_start = alt_nticks();

	while(1){

		/*************** Starting Timer ****************/
		ticks_start = alt_nticks();

		/**************** Drawing the original 16 boxes ****************/
		refreshScreen(pixel_buffer);

		/**************** Receiving input ****************/
		// Wait till there is a message in the FIFO space
		reset_song_list(song_list);
		int z;
		for(z=0;z<16;z++){
			if (alt_up_rs232_get_used_space_in_read_FIFO(uart)!= 0){
				receive_msg(uart,&input_stream,&parity,message,&inputSize);

				/**************** Broadcast and Updating song count ****************/
				check_song(song_list, message[0]);
				button_counter = button_counter + inputSize;
				//printf("button_counter is: %d \n", button_counter);
				send_msg(uart,&input_stream,&parity,message,&inputSize);
			}
			ascii=keyboardInput(keyboard_device,&decode_mode,&temp_buf_Keyboard,&keyEnabled);
				//printf("ascii in main is %c\n",ascii);
				pianoInput=changeToPianoInput(ascii);
				//printf("ascii after change to piano input is %c\n",pianoInput);
				if(pianoInput!='\0'){
						message[0]=pianoInput;
						send_msg(uart,&input_stream,&parity,message,&inputSize);
						//printf("sent msg\n");
					}

				int c;
				for(c=0 ; c<1000000 ; c++);

		}

		// Updating screen display
		printScreen(song_list,pixel_buffer,1);

		/**************** Broadcast z every few seconds  ****************/
		z_end = alt_nticks();
		if ((z_end - z_start) >= (5*ticks_duration)){

			// broadcast z
			message [0] = 'z';
			send_msg(uart,&input_stream,&parity,message,&inputSize);

			// reset z_start
			z_start = alt_nticks();
		}

		/**************** Swap background and foreground ****************/
		alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
		// @EFFECTS:	Swap background and foreground buffers
		while (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
		// @EFFECTS: 	Wait for the swap to complete

		/**************** Waiting for Timer to Expire ***************/
		ticks_end = alt_nticks();
	}
	return 0;
}

