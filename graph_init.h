/*
 * graph_init.h
 *
 *  Created on: 2014-01-28
 *      Author: 1st
 */

#ifndef GRAPH_INIT_H_
#define GRAPH_INIT_H_


#include <stdio.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>

extern unsigned int pixel_buffer_addr1;
extern unsigned int pixel_buffer_addr2;

alt_up_pixel_buffer_dma_dev* graph_init(alt_up_pixel_buffer_dma_dev* pixel_buffer);
//@effects: Initialize the dma pixel buffer to display graphics

#endif /* GRAPH_INIT_H_ */
