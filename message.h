/*
 * message.h
 *
 *  Created on: 2014-03-26
 *      Author: x9a8
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <altera_up_avalon_rs232.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "music.h"

extern int mute;
extern int inputSize;
extern unsigned char input_stream;		// char used to store one byte read from middleman
extern unsigned char parity;			// char used to store parity. Can use parity to check for errors.
extern unsigned char message[255];		// char array used to store the whole input stream
extern unsigned char clientID;			// char used to store the ID of the Android device
extern volatile int now_playing[16];
extern unsigned int s1_data_counter;
extern unsigned int s2_data_counter;
extern unsigned int s3_data_counter;
extern unsigned int s4_data_counter;
extern unsigned int s5_data_counter;
extern unsigned int s6_data_counter;
extern unsigned int s7_data_counter;
extern unsigned int s8_data_counter;


void receive_msg(alt_up_rs232_dev *uart, unsigned char *input_stream, unsigned char *parity,unsigned char *message,int *inputSize);
//REQUIRE: Message[] must contain the signal received from android devices, uart cannot be null
//MODIFY:  nothing
//EFFECT:  Broadcasts received message to all connected android devices.
//         Be careful when you are sending a string from DE2 to android. A string in C is ended with a null character while
//         a string in Java is not terminated with a null character. So when sending a string from DE2 to Java, ONLY SEND THE CHARACTERS!

void send_msg(alt_up_rs232_dev *uart, unsigned char *input_stream, unsigned char *parity,unsigned char *message,int *inputSize);
//REQUIRE: uart cannot be null, Android must only send 1 char character at a time.
//MODIFY:  nothing
//EFFECT:  receive received message from android devices and store the char character at message[0]

void check_song(int *song_list, unsigned char signal);
//REQUIRE: every entry of song_list has to be false
//MODIFY:  song_list
//EFFECT:  update song_list based on the message received from all the android devices

void reset_song_list(int *song_list);
//REQUIRE: none
//MODIFY:  song_list
//EFFECT:  set every entry in song_list to false. S

#endif /* MESSAGE_H_ */
