#include <stdio.h>
#include <unistd.h>
#include <system.h>
#include <stddef.h>
#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/*********** Including Altera hardware libraries ***********/
#include <altera_up_avalon_character_lcd.h>
#include <altera_up_avalon_video_pixel_buffer_dma.h>
#include <altera_up_avalon_video_character_buffer_with_dma.h>
#include <sys/alt_alarm.h>
#include <sys/alt_timestamp.h>
#include <alt_types.h>
#include <sys/alt_alarm.h>
#include <sys/alt_timestamp.h>
#include <alt_types.h>

extern int song_list[16];

void printScreen(int *song_list,alt_up_pixel_buffer_dma_dev*,int);
//REQUIRE: variable flag to be 1.
//MODIFY:  pixel buffer
//EFFECT:  Based on the bool song_list array, it displays tabs touched by the user on the android devices.
//         Supports multiple tabs that are pressed simultaneously.

void refreshScreen(alt_up_pixel_buffer_dma_dev*);
//REQUIRE: Variable flag to be 1
//MODIFY: pixel buffer
//EFFECT: Reset the screen to a 4 by 4 green boxes.
