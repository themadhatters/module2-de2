/*
 * music.h
 *
 *  Created on: 2014-01-23
 *      Author: 1st//
 */

// GUYS CAN WE PLEASE USE GOOD COMMENTS?

#ifndef MUSIC_H_
#define MUSIC_H_

#define HEADER_SIZE 44
#define SAMPLE_SIZE 95		// # bytes to play in 1 interrupt iteration

// FILE SIZE (reordered)
#define S1_SIZE 88636
#define S2_SIZE 42678
#define S3_SIZE 33116
#define S4_SIZE 47952
#define S5_SIZE 9004
#define S6_SIZE 17598
#define S7_SIZE 36050
#define S8_SIZE 61832
#define S9_SIZE 17324
#define S10_SIZE 30364
#define S11_SIZE 18702
#define S12_SIZE 33964
#define S13_SIZE 113416
#define S14_SIZE 42740
#define S15_SIZE 50096
#define S16_SIZE 50624

#include <system.h>
#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#include <assert.h>
#include <altera_up_avalon_audio.h>
#include <altera_up_avalon_audio_and_video_config.h>    //audio hardware
#include <altera_up_avalon_audio_and_video_config_regs.h>
#include <altera_up_avalon_audio_regs.h>
#include <altera_up_avalon_audio_and_video_config_regs.h>
#include <sys/alt_irq.h>
#include "sd_card.h"

extern int mute;

extern unsigned int sound_data_counter; 	//check if reach end of segment
extern unsigned int* sound_buff;

extern short int signedTwoBytes [16];
extern unsigned int normal_buff[SAMPLE_SIZE];

extern unsigned short s1_sound [S1_SIZE];
extern unsigned int s1_data_counter; 		//check if reach end of segment

extern unsigned short s2_sound [S2_SIZE];
extern unsigned int s2_data_counter; 		//check if reach end of segment

extern unsigned short s3_sound [S3_SIZE];
extern unsigned int s3_data_counter; 		//check if reach end of segment

extern unsigned int s4_sound [S4_SIZE];
extern unsigned int s4_data_counter; 		//check if reach end of segment

extern unsigned int s5_sound [S5_SIZE];
extern unsigned int s5_data_counter; 		//check if reach end of segment

extern unsigned int s6_sound [S6_SIZE];
extern unsigned int s6_data_counter; 		//check if reach end of segment

extern unsigned int s7_sound [S7_SIZE];
extern unsigned int s7_data_counter; 		//check if reach end of segment

extern unsigned int s8_sound [S8_SIZE];
extern unsigned int s8_data_counter; 		//check if reach end of segment

extern unsigned int s9_sound [S9_SIZE];
extern unsigned int s9_data_counter;	//check if reach end of segment

extern unsigned int s10_sound [S10_SIZE];
extern unsigned int s10_data_counter; 	//check if reach end of segment

extern unsigned int s11_sound [S11_SIZE];
extern unsigned int s11_data_counter; 	//check if reach end of segment

extern unsigned int s12_sound [S12_SIZE];
extern unsigned int s12_data_counter; 	//check if reach end of segment

extern unsigned int s13_sound [S13_SIZE];
extern unsigned int s13_data_counter; 	//check if reach end of segment

extern unsigned int s14_sound [S14_SIZE];
extern unsigned int s14_data_counter; 	//check if reach end of segment

extern unsigned int s15_sound [S15_SIZE];
extern unsigned int s15_data_counter; 	//check if reach end of segment

extern unsigned int s16_sound [S16_SIZE];
extern unsigned int s16_data_counter; 	//check if reach end of segment


extern unsigned short int unsignedIntTwoBytes;

extern int header_bit;
extern int finishedPlaying;
extern short int music_handle;
extern unsigned int byte_data [2];
extern unsigned short byte_data_s1 [2];
extern unsigned short byte_data_s2 [2];
extern unsigned short byte_data_s3 [2];
extern unsigned short byte_data_s4 [2];
extern unsigned short byte_data_s5 [2];
extern unsigned short byte_data_s6 [2];
extern unsigned short byte_data_s7 [2];
extern unsigned short byte_data_s8 [2];
extern unsigned short byte_data_s9 [2];
extern unsigned short byte_data_s10 [2];
extern unsigned short byte_data_s11 [2];
extern unsigned short byte_data_s12 [2];
extern unsigned short byte_data_s13 [2];
extern unsigned short byte_data_s14 [2];
extern unsigned short byte_data_s15 [2];
extern unsigned short byte_data_s16 [2];
extern unsigned int end_file_counter;		//check if reach end of music
extern unsigned int end_of_segment;
extern unsigned int end_of_file;

extern volatile int now_playing[16];		// the array sent to audio interrupt
extern int global_bit;

alt_up_audio_dev *  av_config_setup();
	//@effects: Initialize audio device

void audio_irq_init(alt_up_audio_dev *audio_dev);
	//@effects: initialize irq (interrupt request) for audio write

void load_music_header(void);
	//@effects: load the wav file header to music buffer

void close_music(void);
	//@effects: close music file

void load_s1_body(void);
	//@effects: load the s1 wav file to sound buffer

void load_s2_body(void);
	//@effects: load the s2 wav file to sound buffer

void load_s3_body(void);
	//@effects: load the s3 wav file to sound buffer

void load_s4_body(void);
	//@effects: load the s4 wav file to sound buffer

void load_s5_body(void);
	//@effects: load the s5 wav file to sound buffer

void load_s6_body(void);
	//@effects: load the s6 wav file to sound buffer

void load_s7_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s8_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s9_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s10_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s11_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s12_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s13_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s14_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s15_body(void);
	//@effects: load the s7 wav file to sound buffer

void load_s16_body(void);
	//@effects: load the s7 wav file to sound buffer

void test_isr(void* context, alt_u32 id);
	//@effects:

void play_sound(alt_up_audio_dev *audio_dev);
	//@effects: call this function to play music

#endif /* MUSIC_H_ */
