/*
 * sound_helper.c
 *
 *  Created on: 2014-04-06
 *      Author: 1st
 */

#include "sound_helper.h"


void makeByteData(void){
	//@effect: make byte data one time for all 16 sounds

	if (now_playing[0] == 1){
		byte_data_s1[0] = s1_sound[s1_data_counter];
		byte_data_s1[1] = s1_sound[s1_data_counter+1];
		signedTwoBytes[0] = (short) ((byte_data_s1[1] << 8) | byte_data_s1[0]);
		s1_data_counter += 2;
	}
	else{
		byte_data_s1[0] = 0;
		byte_data_s1[1] = 0;
		signedTwoBytes[0] = (short) ((byte_data_s1[1] << 8) | byte_data_s1[0]);
		signedTwoBytes[0] = 0;
	}

	if (now_playing[1] == 1){
		byte_data_s2[0] = s2_sound[s2_data_counter];
		byte_data_s2[1] = s2_sound[s2_data_counter+1];
		signedTwoBytes[1] = (short) ((byte_data_s2[1] << 8) | byte_data_s2[0]);
		s2_data_counter += 2;

	}else{
		byte_data_s2[0] = 0;
		byte_data_s2[1] = 0;
		signedTwoBytes[1] = (short) ((byte_data_s2[1] << 8) | byte_data_s2[0]);

	}

	if (now_playing[2] == 1){
		byte_data_s3[0] = s3_sound[s3_data_counter];
		byte_data_s3[1] = s3_sound[s3_data_counter+1];
		signedTwoBytes[2] = (short) ((byte_data_s3[1] << 8) | byte_data_s3[0]);
		s3_data_counter += 2;
	}else{
		byte_data_s3[0] = 0;
		byte_data_s3[1] = 0;
		signedTwoBytes[2] = (short) ((byte_data_s3[1] << 8) | byte_data_s3[0]);
	}

	if (now_playing[3] == 1){
		byte_data_s4[0] = s4_sound[s4_data_counter];
		byte_data_s4[1] = s4_sound[s4_data_counter+1];
		s4_data_counter += 2;
		signedTwoBytes[3] = (short) ((byte_data_s4[1] << 8) | byte_data_s4[0]);
	}else{
		byte_data_s4[0] = 0;
		byte_data_s4[1] = 0;
		signedTwoBytes[3] = (short) ((byte_data_s4[1] << 8) | byte_data_s4[0]);
	}

	if (now_playing[4] == 1){
		byte_data_s5[0] = s5_sound[s5_data_counter];
		byte_data_s5[1] = s5_sound[s5_data_counter+1];
		s5_data_counter += 2;
		signedTwoBytes[4] = (short) ((byte_data_s5[1] << 8) | byte_data_s5[0]);
	}else{
		byte_data_s5[0] = 0;
		byte_data_s5[1] = 0;
		signedTwoBytes[4] = (short) ((byte_data_s5[1] << 8) | byte_data_s5[0]);
	}

	if (now_playing[5] == 1){
		byte_data_s6[0] = s6_sound[s6_data_counter];
		byte_data_s6[1] = s6_sound[s6_data_counter+1];
		s6_data_counter += 2;
		signedTwoBytes[5] = (short) ((byte_data_s6[1] << 8) | byte_data_s6[0]);
	}else{
		byte_data_s6[0] = 0;
		byte_data_s6[1] = 0;
		signedTwoBytes[5] = (short) ((byte_data_s6[1] << 8) | byte_data_s6[0]);
	}

	if (now_playing[6] == 1){
		byte_data_s7[0] = s7_sound[s7_data_counter];
		byte_data_s7[1] = s7_sound[s7_data_counter+1];
		s7_data_counter += 2;
		signedTwoBytes[6] = (short) ((byte_data_s7[1] << 8) | byte_data_s7[0]);
	}else{
		byte_data_s7[0] = 0;
		byte_data_s7[1] = 0;
		signedTwoBytes[6] = (short) ((byte_data_s7[1] << 8) | byte_data_s7[0]);
	}

	if (now_playing[7] == 1){
		byte_data_s8[0] = s8_sound[s8_data_counter];
		byte_data_s8[1] = s8_sound[s8_data_counter+1];
		s8_data_counter += 2;
		signedTwoBytes[7] = (short) ((byte_data_s8[1] << 8) | byte_data_s8[0]);
	}else{
		byte_data_s8[0] = 0;
		byte_data_s8[1] = 0;
		signedTwoBytes[7] = (short) ((byte_data_s8[1] << 8) | byte_data_s8[0]);
	}

	if (now_playing[8] == 1){
		byte_data_s9[0] = s9_sound[s9_data_counter];
		byte_data_s9[1] = s9_sound[s9_data_counter+1];
		s9_data_counter += 2;
		signedTwoBytes[8] = (short) ((byte_data_s9[1] << 8) | byte_data_s9[0]);
	}else{
		byte_data_s9[0] = 0;
		byte_data_s9[1] = 0;
		signedTwoBytes[8] = (short) ((byte_data_s9[1] << 8) | byte_data_s9[0]);
	}
	if (now_playing[9] == 1){
		byte_data_s10[0] = s10_sound[s10_data_counter];
		byte_data_s10[1] = s10_sound[s10_data_counter+1];
		s10_data_counter += 2;
		signedTwoBytes[9] = (short) ((byte_data_s10[1] << 8) | byte_data_s10[0]);
	}else{
		byte_data_s10[0] = 0;
		byte_data_s10[1] = 0;
		signedTwoBytes[9] = (short) ((byte_data_s10[1] << 8) | byte_data_s10[0]);
	}
	if (now_playing[10] == 1){
		byte_data_s11[0] = s11_sound[s11_data_counter];
		byte_data_s11[1] = s11_sound[s11_data_counter+1];
		s11_data_counter += 2;
		signedTwoBytes[10] = (short) ((byte_data_s11[1] << 8) | byte_data_s11[0]);
	}else{
		byte_data_s11[0] = 0;
		byte_data_s11[1] = 0;
		signedTwoBytes[10] = (short) ((byte_data_s11[1] << 8) | byte_data_s11[0]);
	}
	if (now_playing[11] == 1){
		byte_data_s12[0] = s12_sound[s12_data_counter];
		byte_data_s12[1] = s12_sound[s12_data_counter+1];
		s12_data_counter += 2;
		signedTwoBytes[11] = (short) ((byte_data_s12[1] << 8) | byte_data_s12[0]);
	}else{
		byte_data_s12[0] = 0;
		byte_data_s12[1] = 0;
		signedTwoBytes[11] = (short) ((byte_data_s12[1] << 8) | byte_data_s12[0]);
	}
	if (now_playing[12] == 1){
		byte_data_s13[0] = s13_sound[s13_data_counter];
		byte_data_s13[1] = s13_sound[s13_data_counter+1];
		s13_data_counter += 2;
		signedTwoBytes[12] = (short) ((byte_data_s13[1] << 8) | byte_data_s13[0]);
	}else{
		byte_data_s13[0] = 0;
		byte_data_s13[1] = 0;
		signedTwoBytes[12] = (short) ((byte_data_s13[1] << 8) | byte_data_s13[0]);
	}
	if (now_playing[13] == 1){
		byte_data_s14[0] = s14_sound[s14_data_counter];
		byte_data_s14[1] = s14_sound[s14_data_counter+1];
		s14_data_counter += 2;
		signedTwoBytes[13] = (short) ((byte_data_s14[1] << 8) | byte_data_s14[0]);
	}else{
		byte_data_s14[0] = 0;
		byte_data_s14[1] = 0;
		signedTwoBytes[13] = (short) ((byte_data_s14[1] << 8) | byte_data_s14[0]);
	}
	if (now_playing[14] == 1){
		byte_data_s15[0] = s15_sound[s15_data_counter];
		byte_data_s15[1] = s15_sound[s15_data_counter+1];
		s15_data_counter += 2;
		signedTwoBytes[14] = (short) ((byte_data_s15[1] << 8) | byte_data_s15[0]);
	}else{
		byte_data_s15[0] = 0;
		byte_data_s15[1] = 0;
		signedTwoBytes[14] = (short) ((byte_data_s15[1] << 8) | byte_data_s15[0]);
	}
	if (now_playing[15] == 1){
		byte_data_s16[0] = s16_sound[s16_data_counter];
		byte_data_s16[1] = s16_sound[s16_data_counter+1];
		s16_data_counter += 2;
		signedTwoBytes[15] = (short) ((byte_data_s16[1] << 8) | byte_data_s16[0]);
	}else{
		byte_data_s16[0] = 0;
		byte_data_s16[1] = 0;
		signedTwoBytes[15] = (short) ((byte_data_s16[1] << 8) | byte_data_s16[0]);
	}

}

void normalize(int i){
	//@effect: normalize all inputs

	unsignedIntTwoBytes = (unsigned short)(
			(signedTwoBytes[0] >> 4) +
			(signedTwoBytes[1] >> 4) +
			(signedTwoBytes[2] >> 4) +
			(signedTwoBytes[3] >> 4) +
			(signedTwoBytes[4] >> 4) +
			(signedTwoBytes[5] >> 4) +
			(signedTwoBytes[6] >> 4) +
			(signedTwoBytes[7] >> 4) +
			(signedTwoBytes[8] >> 4) +
			(signedTwoBytes[9] >> 4) +
			(signedTwoBytes[10] >> 4) +
			(signedTwoBytes[11] >> 4) +
			(signedTwoBytes[12] >> 4) +
			(signedTwoBytes[13] >> 4) +
			(signedTwoBytes[14] >> 4) +
			(signedTwoBytes[15] >> 4)
	)>>6;

	// checking for upper and lower bounds
	if((signed)unsignedIntTwoBytes >= (signed short) 0x7fff ){
		unsignedIntTwoBytes = 32767;
	}
	if((signed)unsignedIntTwoBytes <= (signed short) 0x8000 ){
		unsignedIntTwoBytes = 32768;
	}

	if(mute == 0){
		// removing sign extension
		normal_buff[i]=(unsigned int)unsignedIntTwoBytes;
	}
	else{
		normal_buff[i]=0;
	}
}

void resetDataCounters(void){
	//@effect: reset data counter one time for all 16 sounds

	if (s1_data_counter >= S1_SIZE){
		s1_data_counter = 0;
		now_playing[0] = 0;
	}
	if (s2_data_counter >= S2_SIZE){
		s2_data_counter = 0;
		now_playing[1] = 0;
	}
	if (s3_data_counter >= S3_SIZE){
		s3_data_counter = 0;
		now_playing[2] = 0;
	}
	if (s4_data_counter >= S4_SIZE){
		s4_data_counter = 0;
		now_playing[3] = 0;
	}
	if (s5_data_counter >= S5_SIZE){
		s5_data_counter = 0;
		now_playing[4] = 0;
	}
	if (s6_data_counter >= S6_SIZE){
		s6_data_counter = 0;
		now_playing[5] = 0;
	}
	if (s7_data_counter >= S7_SIZE){
		s7_data_counter = 0;
		now_playing[6] = 0;
	}
	if (s8_data_counter >= S8_SIZE){
		s8_data_counter = 0;
		now_playing[7] = 0;
	}
	if (s9_data_counter >= S9_SIZE){
		s9_data_counter = 0;
		now_playing[8] = 0;
	}
	if (s10_data_counter >= S10_SIZE){
		s10_data_counter = 0;
		now_playing[9] = 0;
	}
	if (s11_data_counter >= S11_SIZE){
		s11_data_counter = 0;
		now_playing[10] = 0;
	}
	if (s12_data_counter >= S12_SIZE){
		s12_data_counter = 0;
		now_playing[11] = 0;
	}
	if (s13_data_counter >= S13_SIZE){
		s13_data_counter = 0;
		now_playing[12] = 0;
	}
	if (s14_data_counter >= S14_SIZE){
		s14_data_counter = 0;
		now_playing[13] = 0;
	}
	if (s15_data_counter >= S15_SIZE){
		s15_data_counter = 0;
		now_playing[14] = 0;
	}
	if (s16_data_counter >= S16_SIZE){
		s16_data_counter = 0;
		now_playing[15] = 0;
	}
}
