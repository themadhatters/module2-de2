/*
 * sd_card.h
 *
 *  Created on: 2014-02-09
 *      Author: Hao
 */

#ifndef SD_CARD_H_
#define SD_CARD_H_

#include <stdio.h>
#include <Altera_UP_SD_Card_Avalon_Interface.h>

//#define high_score_buffer_size 1024;

alt_up_sd_card_dev* sd_card_init(alt_up_sd_card_dev *SD_CARD);
//@effects: initialize the sd card interface

void read_high_score(char high_score_buffer[1024]);
//@effects: read the high score from scores.txt

void write_high_score(unsigned char* high_score_buffer, int buffer_size);
//@effects: write the high score to scores.txt

void list_files(void);
//@effects: list all the current files in the SD card

#endif /* SD_CARD_H_ */
