/*
 * keyboard.h
 *
 *  Created on: 2014-04-05
 *      Author: yoonjib
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"
#include <stdio.h>
#include <stdlib.h>

#define A 0x41
#define S 0x53
#define D 0x44
#define F 0x46
#define G 0x47
#define H 0x48
#define J 0x4a


alt_up_ps2_dev * init_keyboard(alt_up_ps2_dev *keyboard_device);

char keyboardInput(alt_up_ps2_dev*,KB_CODE_TYPE*,alt_u8*,int*);

char changeToPianoInput(char ascii);


#endif /* KEYBOARD_H_ */
