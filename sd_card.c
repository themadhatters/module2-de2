/*
 * sd_card.c
 *
 *  Created on: 2014-02-09
 *      Author: Hao
 */


#include "sd_card.h"

alt_up_sd_card_dev* sd_card_init(alt_up_sd_card_dev *SD_CARD){
	//@effects: initialize the sd card interface
	SD_CARD = alt_up_sd_card_open_dev("/dev/SD_Card_Interface");
	int connected = 0;

	if(SD_CARD != NULL){
		while(1) {
			if ((connected == 0) && (alt_up_sd_card_is_Present()) ) {
				printf("Card connected.\n");
				if (alt_up_sd_card_is_FAT16()) {
					printf("FAT16 file system detected.\n");
				} else {
					printf("Unknown file system.\n");
				}
				connected = 1;
				break;
			} else if ((connected == 1) && (alt_up_sd_card_is_Present() == false)) {
				printf("Card disconnected.\n");
				connected = 0;
			}
		}
	}
	else{
		printf("Fail to read SD card! \n");
	}
	return SD_CARD;
}

void read_high_score(char high_score_buffer[1024]){
	//@effects: read the high score from scores.txt

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("SCORE.TXT", 0);
	printf("file_handle: %i \n", file_handle);

	int index = 0;
	while (1)
	{
		high_score_buffer[index] = alt_up_sd_card_read(file_handle);
		if(high_score_buffer[index] == -1)
			break;
		//printf("%c \n", high_score_buffer[index]);
		index++;
	}
	alt_up_sd_card_fclose(file_handle);
}

void write_high_score(unsigned char* high_score_buffer, int buffer_size){
	//@effects: write the high score to scores.txt

	short int file_handle;
	file_handle = alt_up_sd_card_fopen("SCORE.TXT", 0);
	printf("file_handle: %u \n", file_handle);

	if (file_handle == -1)
		printf("Problem creating file. Error %d\n", file_handle);

	else {
		printf("SD Accessed Successfully, writing data... \n");

		// WRITING NEW SCORE
		int i = 0;
		for(i=0 ; i < buffer_size ; i++)
		{
			//printf("Writing %c \n", high_score_buffer[i]);
			if (!alt_up_sd_card_write(file_handle, high_score_buffer[i])){
				printf("Writing to SD Card failed\n");
				break;
			}
		}
		alt_up_sd_card_fclose(file_handle);
		printf("Done writing! \n");
	}
}

void list_files(void){
	//@effects: list all the current files in the SD card
	printf("Listing all files: \n");
	short int i;
	char ListOfFiles[1024];

	i = alt_up_sd_card_find_first("", ListOfFiles);
	printf("%s \n", ListOfFiles);

	while(i != -1 ){
		i = alt_up_sd_card_find_next(ListOfFiles);
		if (i == -1 || i == 1 || i == 2 || i == 3)
			break;
		printf("%s \n", ListOfFiles);
		//printf("%d \n", i);
	}
	printf("Searching Complete \n");
}
